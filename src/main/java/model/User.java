package model;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created by gabriel on 03/05/17.
 */
public class User {

    private static final Pattern EMAIL_REGEXP =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    private String email;
    private String password;
    private String passwordConfirmation;
    private String conditionsApproved;
    private String errorMessage;
    private Date creationDate;

    public User() {

    }

    public User(String email, String password, String passwordConfirmation, String conditionsApproved) {
        this.email = email;
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
        this.conditionsApproved = conditionsApproved;
    }

    public User(String email, Date creationDate) {
        this.email = email;
        this.creationDate = creationDate;
    }

    public String getEmail() {
        return email;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getConditionsApproved() {
        return conditionsApproved;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /***
     * Verify that every field has a correct value
     * @return true if everything is OK, else false
     */
    public boolean checkValues(User user, DataSource dataSource) {
        boolean result = true;
        if(!EMAIL_REGEXP.matcher(email).matches()) {
            errorMessage = "Email address is invalid";
            result =  false;
        }
        else if(password.length() < 8) {
            errorMessage = "Password's length is not sufficient";
            result = false;
        }
        else if(!password.equals(passwordConfirmation)) {
            errorMessage = "Passwords are not identical";
            result = false;
        }
        else if(conditionsApproved == null) {
            errorMessage = "Agreements not accepted";
            result = false;
        }
        if(result) {
            user.setCreationDate(new Date());
            user.insert(dataSource);
        }
        return result;
    }

    public void insert(DataSource dataSource) {
        try(Connection conn = dataSource.getConnection(); PreparedStatement prepStatement = conn.prepareStatement(
                "insert into Adherent(email, motDePasse, dateAdhesion) values (?, ?, ?);")) {
            prepStatement.setString(1, email);
            prepStatement.setString(2, password);
            prepStatement.setDate(3, new java.sql.Date(creationDate.getTime()));
            prepStatement.executeUpdate();
        }
        catch(SQLException e) {
            errorMessage = e.getMessage();
        }
    }

    public ArrayList<User> selectAll(DataSource dataSource) {
        ArrayList<User> result = new ArrayList<>();
        try(Connection conn = dataSource.getConnection(); Statement statement = conn.createStatement()) {
            ResultSet resultSet = statement.executeQuery("select * from Adherent;");
            while(resultSet.next()) {
                result.add(new User(resultSet.getString("email"), resultSet.getDate("dateAdhesion")));
            }
            resultSet.close();
        }
        catch(SQLException e) {
            errorMessage = e.getMessage();
        }
        return result;
    }
}
