package controller;

import model.User;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

/**
 * Created by gabriel on 30/04/17.
 */
@WebServlet("/processForm")
public class FormProcessingServlet extends HttpServlet {

    @Resource(name = "localDB")
    private DataSource dataSource;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        User user = new User(
                request.getParameter("email"),
                request.getParameter("password"),
                request.getParameter("passwordVerification"),
                request.getParameter("approvedCheckbox")
        );

        if(user.checkValues(user, dataSource)) {
            request.setAttribute("user", user);
            request.getRequestDispatcher("confirmation.jsp").forward(request, response);
        }
        else {
            request.setAttribute("user", user);
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
