package controller;

import model.User;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;

/**
 * Created by gabriel on 04/05/17.
 */
@WebServlet("/registeredUsers")
public class DisplayUsersServlet extends HttpServlet {

    @Resource(name = "localDB")
    private DataSource dataSource;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        request.setAttribute("users", new User().selectAll(dataSource));
        request.getRequestDispatcher("allUsers.jsp").forward(request, response);
    }
}
