<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <title>Display all users</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>Email</th>
                <th>Date de création</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="row" items="${users}">
                <tr>
                    <td><c:out value="${row.email}" /></td>
                    <td>
                        <c:out value="${row.creationDate}" />
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>

    <br/>
    <c:if test="${not empty user.errorMessage}">
        <span class="red">Error : ${user.errorMessage} </span>
    </c:if>
</body>
</html>
