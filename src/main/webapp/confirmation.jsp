<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Confirmation message</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
    <p>Votre inscription a bien été prise en compte le <fn:formatDate value="${user.creationDate}" pattern="dd/MM/YYYY à HH:mm"/>
        pour l'adresse mail ${user.email}</p>

    <c:if test="${not empty user.errorMessage}">
        <span class="red">Error : ${user.errorMessage} </span>
    </c:if>

    <br/>
    <a href="registeredUsers">Voir tous les utilisateurs</a>
</body>
</html>
